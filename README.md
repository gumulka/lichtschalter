# ESP32 Light controller

ESPHome powered 24V IO board for my fuse-box

# WIP!

I am currently activly working on this project and nothing is stable!

Please do not use this as reference for anything.

I am currently producing the first prototypes. Let's see if anything is working.

## What is this

This is more of a try for myself to work a bit more with PCB design, then
anything else. I also try to replace my hand-soldered Raspberry Pi based
solution with something more sophisticated and less power hungry.
(although, the energy cost for developing this board far outweighs the
energy-savings I could gain by using the ESP based solution...)

The base idea is, that I have some switches, buttons and hal-sensors around my
home, where I want to know wether they are open or closed and turn on some
external 24V Relais to control the lightning in my home.

Now I also want to use this 24V to power my io board and I prefer ethernet over
Wifi, especially in a metal enclosure like a fuse-box.


## Documentation

I use [KiBot](https://github.com/INTI-CMNB/KiBot) to generate Dokumentation.
It generates the following Output:

 * [Schematics PDF](https://gitlab.com/gumulka/lichtschalter/-/jobs/artifacts/master/raw/Fabrication/Schematic.pdf?job=dokumentation)
 * Image of the front side (see below)
 * 3D Rendering of the board (see below)
 * [Gerber Files](https://gitlab.com/gumulka/lichtschalter/-/jobs/artifacts/master/download?job=gerbers)
   (including drill-files)
 * For jlcpcb the [BOM](https://gitlab.com/gumulka/lichtschalter/-/jobs/artifacts/master/raw/Fabrication/ESP-Licht_bom_jlc.csv?job=pos_bom_jlc)
   and [position file](https://gitlab.com/gumulka/lichtschalter/-/jobs/artifacts/master/raw/Fabrication/ESP-Licht_cpl_jlc.csv?job=pos_bom_jlc)

![3D render](https://gitlab.com/gumulka/lichtschalter/-/jobs/artifacts/master/raw/Fabrication/ESP-Licht-3D_top.png?job=dokumentation)

![Top side](https://gitlab.com/gumulka/lichtschalter/-/jobs/artifacts/master/raw/Fabrication/ESP-Licht-top.png?job=dokumentation)

## SMD vs THT

I choose some parts as THT, where SMD variants whould have also been possible.

For the inductor and capacitors, this was because I did not want to spend the
setup-cost for jlcpcb and had them at hand.
But for the MCP23017, it was because I had them lying around here as DIP parts
and due to the global chip shortage, I could not buy the SMD counterparts.
Having them as smaller chips whould have been lovely, but what can you do?


## Parts

Parts not from JLCPCB

The Board-to-Cable connectors are from my local supplier:

* [AKL012-02](https://www.reichelt.de/fahrstuhlklemme-2-pol-2-5-mm-rm-5-akl-012-02-p36617.html)
* [AKL267-06](https://www.reichelt.de/doppelstockklemme-6-pol-1-5-mm-rm-3-5-akl-267-06-p36623.html)

The Ethernet connector and inductor are from digikey

* [Ethernet Connector](https://www.digikey.de/en/products/detail/w%C3%BCrth-elektronik/74990101212/6598155)
* [Inductor](https://www.digikey.de/en/products/detail/w%C3%BCrth-elektronik/7447720180/3476759)
