color("#555") difference() {
rotate([180,-90,0])
linear_extrude(height=10)
polygon(
    [[0,4],
     [9,4],
     [14.6,2.5],
     [14.6,-2.5],
     [9,-4],
     [9,-6.5],
     [0,-6.5],
     ]);

translate([2.5,0,10])
cylinder($fn=50,h=6,r=2);

translate([7.5,0,10])
cylinder($fn=50,h=6,r=2);

translate([2.5,-5,5])
cube([2.8,8,2.8], center=true);

translate([7.5,-5,5])
cube([2.8,8,2.8], center=true);
}
