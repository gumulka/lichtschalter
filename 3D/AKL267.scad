color("#555") difference() {
translate([12.25,0,0]) rotate([0,-90,0])
linear_extrude(height=12.25)
polygon(
    [[0,0],
     [0,12],
     [6,12],
     [8.45,11],
     [8.45,6.45],
     [14.65,6.45],
     [16.65,5.45],
     [16.65,0],
     [12.65,0],
     [8.45,-0.8],
     [8,0]
     ]);


for(i=[1:3]) {
    translate([i*3.5-1.75,8.3,6])
    cylinder($fn=50,h=6,r=1);
    translate([i*3.5-1.75,12,4])
    cube([2.5,8,2.5], center=true);

    translate([i*3.5,2.8,14])
    cylinder($fn=50,h=4,r=1);
    translate([i*3.5,8,12])
    cube([2.5,8,2.5], center=true);
}

translate([0,2,12.425])
cube([3.5,9,8.25], center=true);
cube([3.5,12,40], center=true);
translate([12.25,12,0])
cube([3.5,12,16.91], center=true);
}
